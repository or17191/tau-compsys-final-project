/* An interactive Picol interpreter. */

#include "interp.h"

#define PICOL_IMPLEMENTATION
#include "picol.h"

#include "uart_utils.h"
#include <xdc/runtime/System.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/BIOS.h>


#define TASKSTACKSIZE   2048
#define UARTSTACKSIZE	256

static Task_Struct task0Struct;
static Char task0Stack[TASKSTACKSIZE];

static Task_Struct uartTaskStruct;
static Char uartStack[UARTSTACKSIZE];

static char commandBuf[MAXSTR] = "";

Event_Handle interp_events = NULL;
static Event_Struct interp_events_structs;


void registerGPIOCommands(picolInterp* i);
void pollGPIOEvents(picolInterp* i);
void registerCryptoCommands(picolInterp* i);
void register_trng_commands(picolInterp* i);
void registerTimerCommands(picolInterp* i);
void pollTimerEvents(picolInterp* i);

void readUartIO(UArg arg0, UArg arg1) {
	if(!uart_init()){
		System_abort("Failed to open uart");
	}
	while(1){
		Event_pend(interp_events, INTERP_CMD_PENDING, 0, BIOS_WAIT_FOREVER);
		uart_puts("picol> ");
		uart_fgets(commandBuf, sizeof(commandBuf));
		Event_post(interp_events, INTERP_CMD_READY);
	}
}

void initUartTask(){
	Task_Params taskParams;
	Task_Params_init(&taskParams);
	taskParams.stackSize = UARTSTACKSIZE;
	taskParams.stack = &uartStack;
	Task_construct(&uartTaskStruct, (Task_FuncPtr)readUartIO, &taskParams, NULL);
}

picolInterp* initInterpeter(){
	picolInterp* i = picolCreateInterp();
	if(i == NULL){
		System_abort("Failed to create interpeter");
	}
	registerGPIOCommands(i);
	registerCryptoCommands(i);
	register_trng_commands(i);
	registerTimerCommands(i);
	picolSetVar(i, "argv",  "");
    picolSetVar(i, "argc",  "0");
    picolSetVar(i, "auto_path", "");
    /* The array ::env is lazily populated with the environment variables'
       values. */
	#if PICOL_FEATURE_ARRAYS
		picolEval(i, "array set env {}");
	#endif
	return i;
}

void initEvents(){
	Event_Params params;
	Event_Params_init(&params);
	Event_construct(&interp_events_structs, &params);
	interp_events = Event_handle(&interp_events_structs);
}

void interp(UArg arg0, UArg arg1) {
    picolInterp* i = initInterpeter();
    int rc = 0;
    UInt events;
    initUartTask();
    initEvents();
    System_printf("Starting loop\n");
	System_flush();

	Event_post(interp_events, INTERP_CMD_PENDING);
	while (1) {
		events = Event_pend(interp_events, 0, INTERP_INPUT_EVENTS, BIOS_WAIT_FOREVER);
		if(events & INTERP_GPIO_FIRED){
			pollGPIOEvents(i);
		}
		if(events & INTERP_TIMER_FIRED){
			pollTimerEvents(i);
		}
		if(events & INTERP_CMD_READY){
			rc = picolEval(i, commandBuf);
			if (i->result[0] != '\0' || rc != PICOL_OK) {
				uart_printf("[%d] %s\r\n", rc, i->result);
			}
			Event_post(interp_events, INTERP_CMD_PENDING);
		}
	}
}

void initPicolTask(){
	Task_Params taskParams;
	Task_Params_init(&taskParams);
	taskParams.stackSize = TASKSTACKSIZE;
	taskParams.stack = &task0Stack;
	Task_construct(&task0Struct, (Task_FuncPtr)interp, &taskParams, NULL);
}
