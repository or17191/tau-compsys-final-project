#include "picol.h"
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/knl/Semaphore.h>

#include "Board.h"
#include "interp.h"
#include "callback.h"

#define COUNTOF(x) (sizeof(x)/sizeof((x)[0]))

#define TIMER_COUNT 4

typedef struct _timer_interrupt{
	Semaphore_Handle fired;
	char command[MAXSTR];
	Clock_Struct clock;
	Bool one_shot;
} ClockInterruptHandler;

static ClockInterruptHandler clock_interrupt_handlers[TIMER_COUNT];

static void clear_clock_interrput(ClockInterruptHandler* handler, Bool force){
	Bool was_alive = FALSE;
	if(handler->fired != NULL){
		Semaphore_delete(&handler->fired);
		was_alive = TRUE;
	}
	if(force || was_alive){
		Clock_destruct(&handler->clock);
	}
	handler->command[0] = '\0';
	handler->one_shot = FALSE;
}

static void clockFxn(UArg arg){
	ClockInterruptHandler* handler = (ClockInterruptHandler*)arg;
	if(handler->fired == NULL){
		return;
	}
	Semaphore_post(handler->fired);
	Event_post(interp_events, INTERP_TIMER_FIRED);
}

static int set_timer_interrput(ClockInterruptHandler* handler, char* command, UInt timeout, UInt period){
	Semaphore_Params sem_params;
	Clock_Params clock_params;
	strncpy(handler->command, command, sizeof(handler->command));
	handler->command[sizeof(handler->command)-1] = '\0';
	Clock_Params_init(&clock_params);
	clock_params.arg = (UArg)handler;
	clock_params.period = period;
	clock_params.startFlag = TRUE;
	Clock_construct(&handler->clock, clockFxn, timeout, &clock_params);
	handler->one_shot = (period == 0);
	Semaphore_Params_init(&sem_params);
	sem_params.mode = Semaphore_Mode_BINARY;
	handler->fired = Semaphore_create(0, &sem_params, NULL);
	if(handler->fired == NULL){
		clear_clock_interrput(handler, TRUE);
		return FALSE;
	}
	return TRUE;
}


static UInt millis_to_ticks(UInt millis){
	return (millis * 1000) / Clock_tickPeriod;
}

static ClockInterruptHandler* get_available_handler(){
	int i;
	for(i =0; i < TIMER_COUNT; i++){
		if(clock_interrupt_handlers[i].fired == NULL){
			return &clock_interrupt_handlers[i];
		}
	}
	return NULL;
}

static COMMAND(clock_set){
	UInt timeout, period;
	ClockInterruptHandler* handler;
	char* command = NULL;
	ARITY2((argc <= 4 && argc >= 3), "clock_set command timout [period]");
	command = argv[1];
	SCAN_INT(timeout, argv[2]);
	if(argc < 4){
		period = 0;
	} else{
		SCAN_INT(period, argv[3]);
	}
	handler = get_available_handler();
	if(handler == NULL){
		return picolErr(i, "no clear item");
	}
	if(strlen(command) > sizeof(handler->command) - 1){
		return picolErr(i, "Command is too long");
	}
	if(!set_timer_interrput(handler, command, millis_to_ticks(timeout), millis_to_ticks(period))){
		return picolErr(i, "Couldn't set interrupt");
	}
	return picolSetIntResult(i, handler - clock_interrupt_handlers);
}

static COMMAND(clock_clear){
	UInt clock_num;
	ARITY2(argc == 2, "clock_clear clock_num");
	SCAN_INT(clock_num, argv[1]);
	if(clock_num >= TIMER_COUNT){
		return picolErr(i, "Could not clear clock");
	}
	clear_clock_interrput(&clock_interrupt_handlers[clock_num], FALSE);
	return picolSetResult(i, "");
}

void registerTimerCommands(picolInterp* i){
	picolRegisterCmd(i, "clock_set", picol_clock_set, NULL);
	picolRegisterCmd(i, "clock_clear", picol_clock_clear, NULL);
}

void pollTimerEvents(picolInterp* i){
	ClockInterruptHandler* handler;
	int count;
	int rc;
	for(count=0; count<TIMER_COUNT; count++){
		handler = &clock_interrupt_handlers[count];
		if(handler->fired == NULL || !Semaphore_pend(handler->fired, BIOS_NO_WAIT)){
			continue;
		}
		rc = invokeCallback(i, handler->command);
		if(handler->one_shot){
			clear_clock_interrput(handler, FALSE);
		}
		if (rc != PICOL_OK) {
			printf("[%d] %s\r\n", rc, i->result);
		}
	}
}


