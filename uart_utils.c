#include "uart_utils.h"

#include <ti/drivers/UART.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "Board.h"

static UART_Handle uartHandle=NULL;

void uart_fgets(char* buffer, unsigned int size){
	char *pos;
	char *end = buffer + size - 1;
	char c;
	for(pos=buffer; pos != end; ++pos){
		UART_read(uartHandle, pos, 1);
		c = *pos;
		if(c == '\n' || c == '\r'){ // TODO this is a workaround
			break;
		}
		if(c == 8){ // This should handle backspace
			pos -= 2; // Move pos to one before.
			if(pos < buffer - 1){
				pos = buffer - 1;
			} else{
				uart_putchar(c);
			}
		} else{
			uart_putchar(c);
		}
	}
	uart_puts("\r\n");
	*pos = '\0';
}

void uart_printf(char * format, ...){
	static char buffer[256];
	int len;
	va_list args;
	va_start (args, format);
	len = vsnprintf(buffer,sizeof(buffer),format, args);
	if(len >= sizeof(buffer)){
		len = sizeof(buffer) - 1;
	}
	UART_write(uartHandle, buffer, len);
	va_end (args);
}

void uart_puts(char* str){
	UART_write(uartHandle, str, strlen(str));
}

void uart_putchar(char c){
	UART_write(uartHandle, &c, 1);
}

int uart_init(){
	UART_Params params;
	UART_Params_init(&params);
	params.readReturnMode = UART_RETURN_FULL;
	params.readDataMode = UART_DATA_BINARY;
	params.writeDataMode = UART_DATA_BINARY;
	params.readEcho = UART_ECHO_OFF;
	params.baudRate = 9600;

	uartHandle = UART_open(Board_UART0, &params);
	return uartHandle != NULL;
}

void uart_close(){
	UART_close(uartHandle);
}
