#ifndef UART_UTILS_H
#define UART_UTILS_H

void uart_fgets(char*, unsigned int);
void uart_printf(char*, ...);
void uart_puts(char* str);
void uart_putchar(char c);
int uart_init();
void uart_close();

#endif
