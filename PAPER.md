# Advanced Computer Systems - prof. Sivan Toledo

**Students: Or Ostrovsky, Ofek Kirzner**

**Porting picol to CC2650 Launchpad**

## Abstract
As our final project in the course, we aimed to simplify the task of programming useful software, and to ease the process of interfacing with the CC2650 Launchpad board, made by Texas Instruments. Throughout the course, we have put a lot of efforts to accomplish simple tasks, such as manipulating the input and output pins, responding to interrupts, and utilizing various modules embedded in the processor. This is due to the need to understand the internals of the board, and the need to compile, load and debug one's code. We have come to realize that like scripting languages simplify PC programming, it can simplify embedded programming. And so, we have decided to port the TCL language to the CC2650 Launchpad MCU.

## Capabilities and main functions

* A fully functional TCL interpreter that runs on the MCU. The interpreter supports most relevant language features.
* Controlling said interpreter via UART.
* Controlling the LEDs via the GPIO module.
* Defining interrupt handlers in TCL that execute when either buttons are pushed.
* Using functions exported by the TRNG and CRYPTO modules.

## Choosing an interpreter
The platform at hand required the interpreter to meet the following requirements:

* Very small memory requirements - Since the board has only 28KB of RAM, the interpreter must use as little memory as possible.
* Relatively simple to program - Our main goal was to simplify embedded programming. So, the selected language must be at least as convenient as the C language.
* Easy to embed in C code - We had to integrate the code inside a TI project, so we wanted the interpreter to be easily embeddable.

Apparently, most scripting engines have taken into account the last requirement, and may be easily embedded inside existing code bases.
The other requirements are not so simple, since the more complex and usable the language, so it requires more memory. This rules
out most common languages, like python or ruby. Even Lua, which is considered memory efficient, requires at least 32KB of RAM to operate.
We briefly examined FORTH, which is a very small language that requires few KB's of RAM to function, but we have decided that any
meaningful program written in it, will be more complex than the equivalent C code.

The perfect interpreter (and language) was Picol. Picol is a micro-implementation of the TCL language,
which supports the core features of the language. It is also a single file implementation, and easily
allows to add new commands if the need arises.

The homepage of the picol project is [here](https://chiselapp.com/user/dbohdan/repository/picol/index).

## Interpreter modifications
The biggest challenge we faced, was the fact that the board has a very small amount of RAM, only 28KB.
To address this challenge we did several adjustments to the engine:

* Decreasing static buffers.
* Decreasing max size of strings.
* Not supporting irrelevant commands, like get PID (process id) and files manipulation.
* Using static buffers instead of dynamically allocating (which will exhaust the heap).
* Reducing stack usage, by not using `sprintf` (`ltoa` is more economical), and by using static buffers.
* Reducing heap usage by allocating the 'TCL commands to implementation mapping' statically, instead of dynamically.
* Changing the `if` statement syntax in order to simplify code execution - Regular TCL allows syntactic sugar
inside the `if` statement condition. Most TCL commands look like `!= $a $b`, but `if` statements
allow for the more convenient `$a != $b` syntax. But, since this syntactic sugar requires more memory, we disabled it.

## Responding to multiple IO sources

We wanted the interpreter to execute commands upon input from one of many sources.
The interpreter should wait until input arrives from any source, and execute the relevant code.
That code might send textual output via UART, or change the state of the board hardware in another way.
Currently, we support three sources:
* The UART input
* GPIO interrupts
* Clock generated interrupts

### Implementation

Since the interpreter doesn't support multithreading, only one task may use it at once.
Also, since it requires a large stack, only one task may use it at all.
This results in the following architecture:

* A single task executes the TCL commands. That task waits using `Event_pend` on a `tirtos` `Event`.
Any task may post events, which will cause main task to stop waiting and to execute the relevant code
for that event.
* Another task reads characters from the UART until it reaches the line is over. When that happens, the appropriate
event is posted (using `Event_post`), and the main task executes the command from, which is stored in a global buffer.
* When a GPIO interrupt is triggered, an event is posted. The interrupt also posts another semaphore, which is attached
to a TCL interrupt handler, which is a TCL code line. Then, the main task executes any handler whose semaphore has been
posted.
* The same holds for clock interrupt handlers.

Using `tirtos`'s `Event`s allow to add a large number of input sources in the future, since it supports up to 32
distinct events.
Also, since all event sources are handled equally without any busy-waiting, the interpreter is responsive for all of them.


## New Commands
After Connecting using a UART (using putty for example), with 9600 baud rate, one can try the following commands:

1. `setgpio <pin id> <value>` - Sets pin id to the given value.
1. `getgpio <pin id>` - Returns the value of the pin id.
1. `gpio_set_inter <pin id> <tcl function>` - Sets an interrupt handler on the given pin id.
1. `gpio_clear_inter <pin id>` - Deletes the interrupt handler previously set on the given pin id.
1. `sha256 <value>` - Computes the SHA256 of the given value.
1. `aes_set_iv <iv>` - Sets the iv of a following AES encryption.
1. `aes_set_key <key>` - sets the key of a following AES encryption.
1. `aes_cbc_enc <value>` - Returns the AES encryption of a given string.
1. `aes_cbc_dec <value>` - Returns the decryption of a given string.
1. `true_rand <n>` - Returns a truly random number in the range 0-n.
1. `true_randstr <n>` - Returns a true random string of length n.
1. `clock_set <command> <timeout> <period?>` - Creates a clock that executes the command after the timeout.
	If a period is specified, the command will be executed again after that period. The command returns the
	number of the clock used. Both times are specified in milliseconds.
1. `clock_clear <clock_id>` - Clears the clock with the given id, which is returned by the previous commend.

*All gpio commands currently support only LED0, LED1, BUTTON0 and BUTTON1, but may be expanded in the future.*

## Demonstration
All demonstrations are also available in the `DEMO.tcl` file.

### General tcl commands

	+ 1 1

	set x 0
	incr x
	puts $x

	if {!= $x 0} {puts "hello"} else {puts "world"}

	for {set i 0} {< $i 5} {incr i} {puts [* 2 $i]}

	proc foo {arg1} {puts $arg1}
	foo "me"


### TRGN/ENC/DEC usage

	set key [true_randstr 16]
	set iv [true_randstr 16]

	aes_set_key $key
	aes_set_iv $iv

	set crypt [aes_cbc_enc "Hello world"]
	aes_cbc_dec $crypt

	sha256 "hello world"

### GPIO

	setgpio 6 1
	getgpio 6
	setgpio 6 0

	proc flip_gpio {pin} {set x [getgpio $pin]; setgpio $pin [! $x]}

	gpio_set_inter 13 {flip_gpio 6}
	gpio_set_inter 14 {flip_gpio 7}

	set pushes 0

	gpio_clear_inter 13
	gpio_set_inter 13 {uplevel 1 {incr pushes}}

### Clock

	clock_set {flip_gpio 6} 1000  1000
	clock_clear 0

