#include "picol.h"
#include <ti/sysbios/BIOS.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/pin/PINCC26XX.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/knl/Semaphore.h>

#include "Board.h"
#include "interp.h"
#include "callback.h"

#define COUNTOF(x) (sizeof(x)/sizeof((x)[0]))

typedef struct _gpio_interrupt{
	Semaphore_Handle fired;
	char command[MAXSTR];
} GPIOInterruptHandler;


static PIN_Config pinTable[] = {
    Board_LED0 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW  | PIN_PUSHPULL | PIN_DRVSTR_MAX,
	Board_BUTTON0  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
	Board_BUTTON1  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    PIN_TERMINATE
};

static PIN_Handle pinHandle;
static PIN_State pinState;

static GPIOInterruptHandler gpio_interrupt_handlers[COUNTOF(pinTable)-1] = {0};

static void clear_gpio_interrput(GPIOInterruptHandler* handler){
	if(handler->fired != NULL){
		Semaphore_delete(&handler->fired);
	}
	handler->command[0] = '\0';
}

static int set_gpio_interrput(GPIOInterruptHandler* handler, char* command){
	Semaphore_Params params;
	strncpy(handler->command, command, sizeof(handler->command));
	handler->command[sizeof(handler->command)-1] = '\0';
	Semaphore_Params_init(&params);
	params.mode = Semaphore_Mode_BINARY;
	handler->fired = Semaphore_create(0, &params, NULL);
	if(handler->fired == NULL){
		clear_gpio_interrput(handler);
		return FALSE;
	}
	return TRUE;
}

static PIN_Config* get_config(PIN_Id pinId){
	PIN_Config* cfg;
	for(cfg=pinTable; *cfg != PIN_TERMINATE; cfg++){
		if(PIN_ID(*cfg) == pinId){
			break;
		}
	}
	return cfg;
}

static void pinFxn(PIN_Handle handle, PIN_Id pinId){
	GPIOInterruptHandler* handler;
	PIN_Config* cfg = get_config(pinId);
	if(*cfg == PIN_TERMINATE){
		return;
	}
	handler = &gpio_interrupt_handlers[cfg-pinTable];
	if(handler->fired == NULL){
		return;
	}
	Semaphore_post(handler->fired);
	Event_post(interp_events, INTERP_GPIO_FIRED);
}

static COMMAND(setgpio){
    /* This is an example of how to wrap int functions. */
    PIN_Id pin_id;
	int pin_value;
    ARITY2(argc == 3, "setgpio pin_number value");
    SCAN_INT(pin_id, argv[1]);
    SCAN_INT(pin_value, argv[2]);
    if(*get_config(pin_id) & PIN_GPIO_OUTPUT_EN == 0){
    	return picolErr(i, "pin is not an output pin");
    }
    PIN_setOutputValue(pinHandle, pin_id, pin_value);
    return picolSetIntResult(i, pin_value);
}

static COMMAND(getgpio){
	PIN_Id pin_id;
	PIN_Config config;
	int pin_value;
	ARITY2(argc == 2, "getgpio pin_number");
	SCAN_INT(pin_id, argv[1]);
	config = *get_config(pin_id);
	if(config & PIN_GPIO_OUTPUT_EN){
		pin_value = PIN_getOutputValue(pin_id);
	} else if(config & PIN_INPUT_EN){
		pin_value = PIN_getInputValue(pin_id);
	} else{
		return picolErr(i, "pin is a not valid pin");
	}
	return picolSetIntResult(i, pin_value);
}

static COMMAND(gpio_set_inter){
	PIN_Id pin_id;
	PIN_Config* config;
	GPIOInterruptHandler* handler;
	char* command = NULL;
	ARITY2(argc == 3, "gpio_set_inter pin_number command");
	SCAN_INT(pin_id, argv[1]);
	command = argv[2];
	config = get_config(pin_id);
	if(*config == PIN_TERMINATE){
		return picolErr(i, "pin is a not valid pin");
	}
	handler = &gpio_interrupt_handlers[config-pinTable];
	if(handler->fired){
		return picolErr(i, "interrupt is already set");
	}
	if(strlen(command) > sizeof(handler->command) - 1){
		return picolErr(i, "command is too long");
	}
	if(!set_gpio_interrput(handler, command)){
		return picolErr(i, "couldn't set interrupt");
	}
	return picolSetResult(i, "");
}

static COMMAND(gpio_clear_inter){
	PIN_Id pin_id;
	PIN_Config* config;
	ARITY2(argc == 2, "gpio_clear_inter pin_number");
	SCAN_INT(pin_id, argv[1]);
	config = get_config(pin_id);
	if(*config == PIN_TERMINATE){
		return picolErr(i, "pin is a not valid pin");
	}
	clear_gpio_interrput(&gpio_interrupt_handlers[config-pinTable]);
	return picolSetResult(i, "");
}

static void initGPIOPins(){
    /* Open LED pins */
    pinHandle = PIN_open(&pinState, pinTable);
    if(!pinHandle) {
        System_abort("Error initializing board pins\n");
    }
    if (PIN_registerIntCb(pinHandle, &pinFxn) != 0) {
		System_abort("Error registering callback function\n");
	}
}

void registerGPIOCommands(picolInterp* i){
	initGPIOPins();
	picolRegisterCmd(i, "setgpio", picol_setgpio, NULL);
	picolRegisterCmd(i, "getgpio", picol_getgpio, NULL);
	picolRegisterCmd(i, "gpio_set_inter", picol_gpio_set_inter, NULL);
	picolRegisterCmd(i, "gpio_clear_inter", picol_gpio_clear_inter, NULL);
}

void pollGPIOEvents(picolInterp* i){
	GPIOInterruptHandler* handler;
	int count;
	int rc;
	for(count=0; count<COUNTOF(gpio_interrupt_handlers); count++){
		handler = &gpio_interrupt_handlers[count];
		if(handler->fired == NULL || !Semaphore_pend(handler->fired, BIOS_NO_WAIT)){
			continue;
		}
		rc = invokeCallback(i, handler->command);
		if (rc != PICOL_OK) {
			printf("[%d] %s\r\n", rc, i->result);
		}
	}
}
