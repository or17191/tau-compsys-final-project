/*
 * interp.h
 *
 *  Created on: Feb 4, 2017
 *      Author: orost
 */

#ifndef INTERP_H_
#define INTERP_H_

#include <ti/sysbios/knl/Event.h>

extern Event_Handle interp_events;

#define INTERP_CMD_READY Event_Id_00
#define INTERP_CMD_PENDING Event_Id_01
#define INTERP_GPIO_FIRED Event_Id_02
#define INTERP_TIMER_FIRED Event_Id_03

#define INTERP_INPUT_EVENTS (INTERP_CMD_READY | INTERP_GPIO_FIRED | INTERP_TIMER_FIRED)

#endif /* INTERP_H_ */
