#include <driverlib/trng.h>
#include "picol.h"
#include "hex_utils.h"
#include <ti/drivers/power/PowerCC26XX.h>


static void trng_init( void );
static void trng_wait_until_ready( void );
static uint32_t trng_get( void );


static void trng_init( void )
{
    Power_setDependency(PowerCC26XX_PERIPH_TRNG);

	TRNGConfigure( 256, 256, 0x01 );
	TRNGEnable();
}


static void trng_wait_until_ready( void )
{
	while(!(TRNGStatusGet() & TRNG_NUMBER_READY))
	{}
}

static uint32_t trng_get( void )
{
	trng_wait_until_ready();

	return TRNGNumberGet(TRNG_LOW_WORD);
}


static COMMAND(true_rand)
{
	int n;
	ARITY2(argc == 2, "true_rand n (returns a random integer 0..<n)");
	SCAN_INT(n, argv[1]);

	return picolSetIntResult(i, n ? trng_get()%n : trng_get());
}

static COMMAND(true_randstr)
{
	int l, n, rc;
	uint8_t* buf=NULL;
	char* buf_hex=NULL;
	ARITY2(argc == 2, "true_rand n (returns a random string of length)");
	SCAN_INT(n, argv[1]);
	buf = malloc(n);
	buf_hex = malloc(n*2 + 1);
	if(buf == NULL || buf_hex == NULL){
		rc = picolErr(i, "Failed to alloc");
		goto randstr_finish;
	}
	for(l = 0; l < n; l++){
		buf[l] = (uint8_t)(trng_get() >> 24);
	}
	convertToHex(buf, n, buf_hex);
	buf_hex[n*2] = '\0';
	rc = picolSetResult(i, buf_hex);
randstr_finish:
	free(buf);
	free(buf_hex);
	return rc;
}


void register_trng_commands(picolInterp* i){
	trng_init();

	picolRegisterCmd(i, "true_rand", picol_true_rand, NULL);
	picolRegisterCmd(i, "true_randstr", picol_true_randstr, NULL);
}


