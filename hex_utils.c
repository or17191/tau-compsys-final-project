#include "hex_utils.h"

#define AS_HEX(x) ((x) >= 10 ? (x) - 10 + 'A' : (x) + '0')
#define FROM_HEX(x) ((x) >= 'A' ? (x) - 'A' + 10 : (x) - '0')


void convertToHex(uint8_t* src, int len, char* dst){
	uint8_t c;
	uint8_t* end = src + len;
	for(; src < end; ++src){
		c = *src;
		*(dst++) = AS_HEX(c>>4);
		*(dst++) = AS_HEX(c & 15);
	}
}

void convertFromHex(char* src, int len, uint8_t* dst){
	// Assumes len % 2 == 0
	char c;
	uint8_t d;
	char* end = src + len;
	for(; src < end; ++dst){
		c = *(src++);
		d = FROM_HEX(c) << 4;
		c = *(src++);
		d |= FROM_HEX(c);
		*dst = d;
	}
}
