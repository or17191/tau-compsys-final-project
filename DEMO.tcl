# General tcl commands

+ 1 1

set x 0
incr x
puts $x

if {!= $x 0} {puts "hello"} else {puts "world"}

proc foo {arg1} {puts $arg1}
foo "me"


# TRGN/ENC/DEC usage

set key [true_randstr 16]
set iv [true_randstr 16]

aes_set_key $key
aes_set_iv $iv

set crypt [aes_cbc_enc "Hello world"]
aes_cbc_dec $crypt

# GPIO

setgpio 6 1
getgpio 6
setgpio 6 0

proc flip_gpio {pin} {set x [getgpio $pin]; setgpio $pin [! $x]}

gpio_set_inter 13 {flip_gpio 6}
gpio_set_inter 14 {flip_gpio 7}

set pushes 0

gpio_clear_inter 13
gpio_set_inter 13 {uplevel 1 {incr pushes}}

# Clock

clock_set "flip_gpio 6" 1000  1000
clock_clear 0