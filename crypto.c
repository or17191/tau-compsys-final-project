#include "picol.h"
#include "hex_utils.h"
#include <ti/sysbios/BIOS.h>
#include <xdc/runtime/System.h>
#include <driverlib/rom_crypto.h>
#include <driverlib/crypto.h>
#include <ti/drivers/power/PowerCC26XX.h>

#include "Board.h"

#define COUNTOF(x) (sizeof(x)/sizeof((x)[0]))
#define SHA256_DIGEST_LEN (256/8)
#define AES_KEY_SIZE (128/8)
#define AES_BLOCK_SIZE (128/8)

static uint32_t aes_key[AES_KEY_SIZE/4];
static uint32_t aes_iv[AES_BLOCK_SIZE/4];


static COMMAND(sha256){
    SHA256_memory_t sha_state;
    uint32_t len;
    uint8_t output[SHA256_DIGEST_LEN];
    char hexOutput[SHA256_DIGEST_LEN*2 + 1];
    ARITY2(argc == 2, "sha256 value");
    len = strlen(argv[1]);
    SHA256_runFullAlgorithm(&sha_state, (uint8_t*)argv[1], len, output);
    convertToHex(output, SHA256_DIGEST_LEN, hexOutput);
    hexOutput[SHA256_DIGEST_LEN*2] = '\0';
    return picolSetResult(i, hexOutput);
}

static COMMAND(aes_set_iv){
	ARITY2(argc == 2, "aes_set_iv iv");
	if(strlen(argv[1]) != AES_BLOCK_SIZE * 2){
		return picolErr(i, "IV is not of propper length");
	}
	convertFromHex(argv[1], AES_BLOCK_SIZE*2, (uint8_t*)aes_iv);
	return picolSetResult(i, "");
}

static COMMAND(aes_set_key){
	ARITY2(argc == 2, "aes_set_key key");
	if(strlen(argv[1]) != AES_KEY_SIZE * 2){
		return picolErr(i, "Key is not of propper length");
	}
	convertFromHex(argv[1], AES_KEY_SIZE*2, (uint8_t*)aes_key);
	if(CRYPTOAesLoadKey(aes_key, CRYPTO_KEY_AREA_0) != AES_SUCCESS){
		return picolErr(i, "Failed to store key");
	}
	return picolSetResult(i, "");
}

static COMMAND(aes_cbc_enc){
	uint32_t *output=NULL, *input=NULL;
	char *output_hex=NULL;
	int length, padded_len, rc, remainders;
	ARITY2(argc == 2, "aes_cbc_enc value");
	length = strlen(argv[1]);
	remainders = length % AES_BLOCK_SIZE;
	padded_len = length + AES_BLOCK_SIZE - remainders;
	input = malloc(padded_len);
	output = malloc(padded_len);
	output_hex = malloc(padded_len * 2 + 1);
	if(output == NULL || output_hex == NULL || input == NULL){
		rc = picolErr(i, "Can't allocate memory for aes cbc enc");
		goto aes_cbc_enc_finished;
	}
	memcpy(input, argv[1], length);
	memset((uint8_t*)input + length, AES_BLOCK_SIZE - remainders, AES_BLOCK_SIZE - remainders);
	if(CRYPTOAesCbc(input, output, padded_len, aes_iv, CRYPTO_KEY_AREA_0, TRUE, FALSE) != AES_SUCCESS){
		rc = picolErr(i, "Failed to encrypt");
		goto aes_cbc_enc_finished;
	}
	while(CRYPTOAesCbcStatus() != AES_SUCCESS){
		CPUdelay(1000);
	}
	CRYPTOAesCbcFinish();
	convertToHex((uint8_t*)output, padded_len, output_hex);
	output_hex[padded_len * 2] = '\0';
	rc = picolSetResult(i, output_hex);
aes_cbc_enc_finished:
	free(output);
	free(input);
	free(output_hex);
	return rc;
}

static COMMAND(aes_cbc_dec){
	uint32_t *output=NULL, *input=NULL;
	char* output_text=NULL;
	int length, rc;
	ARITY2(argc == 2, "aes_cbc_dec value");
	length = strlen(argv[1])/2;
	input = malloc(length);
	output = malloc(length+1);
	if(output == NULL || input == NULL){
		rc = picolErr(i, "Can't allocate memory for aes cbc enc");
		goto aes_cbc_enc_finished;
	}
	convertFromHex(argv[1], length*2, (uint8_t*)input);
	if(CRYPTOAesCbc(input, output, length, aes_iv, CRYPTO_KEY_AREA_0, FALSE, FALSE) != AES_SUCCESS){
		rc = picolErr(i, "Failed to decrypt");
		goto aes_cbc_enc_finished;
	}
	while(CRYPTOAesCbcStatus() != AES_SUCCESS){
		CPUdelay(1000);
	}
	CRYPTOAesCbcFinish();
	output_text = (char*)output;
	output_text[length-(int)(output_text[length-1])] = '\0';
	rc = picolSetResult(i, output_text);
aes_cbc_enc_finished:
	free(output);
	free(input);
	return rc;
}

void registerCryptoCommands(picolInterp* i){
	Power_setDependency(PowerCC26XX_PERIPH_CRYPTO);

	picolRegisterCmd(i, "sha256", picol_sha256, NULL);
	picolRegisterCmd(i, "aes_cbc_enc", picol_aes_cbc_enc, NULL);
	picolRegisterCmd(i, "aes_cbc_dec", picol_aes_cbc_dec, NULL);
	picolRegisterCmd(i, "aes_set_iv", picol_aes_set_iv, NULL);
	picolRegisterCmd(i, "aes_set_key", picol_aes_set_key, NULL);
}
