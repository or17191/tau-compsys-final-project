/*
 * hex_utils.h
 *
 *  Created on: Jan 14, 2017
 *      Author: orost
 */

#ifndef HEX_UTILS_H_
#define HEX_UTILS_H_

#include <stdint.h>

void convertToHex(uint8_t* src, int len, char* dst);
void convertFromHex(char* src, int len, uint8_t* dst);

#endif /* HEX_UTILS_H_ */
