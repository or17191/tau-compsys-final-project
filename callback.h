/*
 * callback.h
 *
 *  Created on: Feb 24, 2017
 *      Author: orost
 */

#ifndef CALLBACK_H_
#define CALLBACK_H_

#include "picol.h"

int invokeCallback(picolInterp* i, char * command);

#endif /* CALLBACK_H_ */
