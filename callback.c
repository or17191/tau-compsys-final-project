#include "callback.h"
#include <stdlib.h>
#include <string.h>

int invokeCallback(picolInterp* i, char * command){
    char *body=command;
    picolCallFrame* cf = calloc(1, sizeof(*cf));
    int errcode = PICOL_OK;
    if (!cf) {
        printf("could not allocate callframe\r\n");
        exit(1);
    }
    cf->parent   = i->callframe;
    i->callframe = cf;
    if (i->level>MAXRECURSION) {
        return picolErr(i, "too many nested evaluations (infinite loop?)");
    }
    i->level++;
    cf->command = strdup(command);
    errcode     = picolEval(i, body);
    if (errcode == PICOL_RETURN) {
        errcode = PICOL_OK;
    }
    /* remove the called proc callframe */
    picolDropCallFrame(i);
    i->level--;
    return errcode;
}


